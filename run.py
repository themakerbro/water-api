#!/usr/bin/python
# coding: utf-8
""" Filename:     run.py
    Descritpion:  This file runs the Flask application service
    Requirements: Flask
    Author:       MakerBro
"""
from webapp.application import app
import os

if __name__ == '__main__':
    app.run(host="0.0.0.0",
        port=int(os.getenv('VCAP_APP_PORT', '5000')),
        debug=True,
    )

