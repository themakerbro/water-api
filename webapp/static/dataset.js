function getEvents(){

	return [
		{
			id : 'E01',
			title : '',
			start : '05-01-1970 00:00:00',
			end : '05-01-1970 07:00:00',
			backgroundColor: 'green',
			textColor : '#FFF'
		},
		{
			id : 'E01',
			title : '',
			start : '05-01-1970 22:00:00',
			end : '05-01-1970 24:00:00',
			backgroundColor: 'green',
			textColor : '#FFF'
		},
		{
			id : 'E02',
			title : '',
			start : '07-01-1970 00:00:00',
			end : '07-01-1970 07:00:00',
			backgroundColor: 'green',
			textColor : '#FFF'
		},
		{
			id : 'E02',
			title : '',
			start : '07-01-1970 22:00:00',
			end : '07-01-1970 24:00:00',
			backgroundColor: 'green',
			textColor : '#FFF'
		},
	];
}