#!/usr/bin/python
# coding: utf-8
"""
    Filename:     views.py
    Description:  This file is one of the views file that can contain the
                  routes for the application
    Requirements:
    Author:       MakerBro
"""
import os
import csv
import json
from pprint import pprint
import flask
from flask import Response, request
from application import app
from settings import APP_STATIC, APP_DATA

# importing application wide parameters and global variables that have been
# defined in application.py
message = app.config['HELLO_WORLD']

def data_read():
    with open(os.path.join(APP_DATA, 'data.csv')) as f:
        data = f.readlines()
        fieldnames = ('city','county','url')
        reader = csv.DictReader(data, fieldnames)
        json_str = json.dumps( list(reader) )
    return json.loads(json_str)

@app.route('/')
def home_page():
    return flask.render_template('main.html', data=data_read())

@app.route('/data.json')
def data_json():
    with open(os.path.join(APP_DATA, 'data.json')) as f:
        data_str = f.read()
        if len(data_str) > 0:
            response_json = json.loads(data_str)
        else:
            response_json = "{}"
        return Response(json.dumps(response_json, sort_keys=True, indent=4),
                status=200,
                mimetype="application/json")

@app.route('/list')
def list_page():
    zipcode = request.args.get('zip')
    with open(os.path.join(APP_DATA, 'data.json')) as f:
        restrictions = json.loads(f.read())
        if zipcode is None:
            return flask.render_template('list.html', data=restrictions)
        zip_restrictions = list()
        for restriction in restrictions:
            if restriction['zip'] == zipcode:
                zip_restrictions.append(restriction)
        if len(zip_restrictions) > 0:
            return flask.render_template('list.html', data=zip_restrictions)
        else:
            return flask.redirect(flask.url_for('nozip', zip=zipcode))

@app.route('/new', methods=['GET', 'POST'])
def new_page():
    if request.method == 'GET':
        return flask.render_template('new.html')
    elif request.method == 'POST':
        restriction = request.form.to_dict()
        restrictions = list()
        with open(os.path.join(APP_DATA, 'data.json'), mode='r') as f:
            data_str = f.read()
            if len(data_str) > 0:
                restrictions = json.loads(data_str)
        restriction['id'] = len(restrictions) + 1
        restrictions.append(restriction)
        with open(os.path.join(APP_DATA, 'data.json'), mode='w') as f:
            json.dump(restrictions, f)
            return flask.redirect(flask.url_for('view_page', zip=restriction['zip']))

@app.route('/dev')
def dev():
        return flask.render_template('dev.html')

@app.route('/contact', methods=['GET', 'POST'])
def contact():
    if request.method == 'GET':
        return flask.render_template('contact.html')

@app.route('/nozip')
def nozip():
    zipcode = request.args.get('zip')
    return flask.render_template('nozip.html', zip=zipcode)

@app.route('/calendar')
def calendar():
    return flask.render_template('calendar.html')

@app.route('/view')
def view_page():
    zipcode = request.args.get('zip')
    if zipcode is None:
      return flask.redirect(flask.url_for('list_page'))
    with open(os.path.join(APP_DATA, 'data.json')) as f:
        restrictions = json.loads(f.read())
        zip_restrictions = list()
        for restriction in restrictions:
            if restriction['zip'] == zipcode:
                zip_restrictions.append(restriction)
        if len(zip_restrictions) > 0:
            return flask.render_template('view.html', data=zip_restrictions[0])
        else:
            return flask.redirect(flask.url_for('nozip', zip=zipcode))

