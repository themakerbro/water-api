# README #

Communities around the world are in an extreme drought and governments have created water irrigation restrictions to reduce water consumption. However, the water restrictions for each city are hard to find, confusing, and not centralized. Home owners want a user-friendly and automated system to select their location, send the data to their sprinkler system, and remain in compliance even when the requirements change.

Enter WaterSpec.

WaterSpec is a centralized repository of water restriction data for the entire U.S. so that home owners can easily select their location using GPS and send the restriction data directly to any sprinkler system. This open API will allow cities or citizens (using crowd-sourcing) to update the restrictions and send that data directly to the each device ensuring constant compliance with ever-changing regulations. The best way to ensure a green lawn and no water fines is to use the WaterSpec app to automate your sprinkler system for you.

Website:

https://water-api.cfapps.io



Presentation:

https://docs.google.com/a/robotnyc.com/presentation/d/1eO98SlduYGCtzDML2u_W_l4QGaIYmowOwgNWiYHtw7Q/edit?usp=sharing

### What is this repository for? ###

* This repository hosts the Python Flask web application which serves both the user front-end and data API.

### How do I get set up? ###

* Run Server

# `$ virtualenv env`
# `$ source env/bin/activate`
# `$ pip install -r requirements.txt`
# `$ python ./run.py`

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact